// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCkVocd1M1e84LscH-P8OMWSVoRPyrdrgM",
  authDomain: "app-tavoli.firebaseapp.com",
  databaseURL: "https://app-tavoli.firebaseio.com/",
  //projectId: "app-tavoli",
  storageBucket: "app-tavoli.appspot.com",
  //messagingSenderId: "33041284859",
  //appId: "1:33041284859:web:d04cd77593dff5665afa5e"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

init($("body").attr("name"));

function init(pageName) {
    activeHamb();
    if(pageName === "home") {
        writeData();
    } else {
        getData();
    }
}

function writeData() {
    firebase.database().ref("TavoloAssegnato" + "_" + document.getElementById('input_2').value).set({
    cognome: document.getElementById('input_1').value,
    nTav: document.getElementById('input_2').value,
    })
}

function activeHamb() {
    $(".hamburgher").on('click', function(e){
         $(".tendina").toggleClass('is-open');
         e.preventDefault();
     });
     $(".hamburgher").on('click', function(e){
         $(".hamburgher").toggleClass('change_color');
         e.preventDefault();
     });
}

function getData() {
    var i = 0;
    firebase.database().ref('/').once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot)
        {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                var html = '<div class="cell"><p id="Data-'+i+'"></p></div><div class="cell" name="nTav"><p id="Data2-'+i+'"></p>'
                $(".tab").append(html);

                document.getElementById("Data-"+i).innerHTML = childData['cognome']
                document.getElementById("Data2-"+i).innerHTML = childData['nTav'];

                i++
        })
    })
};
